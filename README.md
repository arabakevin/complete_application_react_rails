# README

*complete_application_react_rails*

############### Installation ##############

```
git clone git@gitlab.com:arabakevin/complete_application_react_rails.git
npm install
```

############### Starting the different application  ##############

* to start the rails application

```
bin/rails s -p 3001
```
The react application used the port 3001.

http://localhost:3001

* to start the react application

```
npm start
```

The react application used the port 3000.

http://localhost:3000


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
