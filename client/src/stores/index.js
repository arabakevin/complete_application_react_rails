import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import rootReducer from '../reducers'
import thunkMiddleware from 'redux-thunk'

const loggerMiddleware = createLogger()

// we have store who manage the state of global application
export const store = createStore(
  rootReducer,
  // What this will do is log to your console everytime an Action is dispatched.
  // It'll log the action, as well as the payload.
  applyMiddleware(
    //Redux Thunk middleware allows you to write action creators that return
    //a function instead of an action.
    //The thunk can be used to delay the dispatch of an action, or to dispatch
    //only if a certain condition is met. The inner function receives the store
    //methods dispatch and getState as parameters.
    thunkMiddleware,
    // This one is for display the log
    loggerMiddleware
  )
)
