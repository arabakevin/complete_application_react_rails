/**
ROUTER OF THE APPLICATION
**/
import React from 'react'
import { createBrowserHistory } from 'history'
import {Route, Router} from 'react-router'

import DeviseRegistrationsNew from '../views/devise/registrations/DeviseRegistrationNew'

// to explain why we use ```({component: Component, ...rest})```
// https://stackoverflow.com/questions/43484302/what-does-it-mean-rest-in-react-jsx
// copy component in a new variable Component and without component we assign
// a other variable rest.
export const AppRouter = ({component: Component, ...rest}) => (

// Without browserHistory, when you navigate to other URL except / like /posts/1
// and refresh the page.
// The server only gives you the home page.
// With browserHistory, the router will handle those URL properly.

<Router history = {createBrowserHistory()}>
  <div>
    <Route path="/users/registration" component={DeviseRegistrationsNew} />
  </div>
</Router>
)
