import { userService } from '../services/user.service'


export const userActions = {
  register
}

function register(user) {
  // here we initialize the result
  // dispatch ==> distributes our action to the store and its reducers.
  return dispatch => {
    dispatch({ type: 'RegisterRequestUser', user })

    // here we call register from userService
    userService.register(user)
      // if success
      .then(
        user => {
          // we call the reducer with the type indicated
          dispatch({ type: 'RegisterSuccessUser', user })
        },
        // else
        error => {
          // we call the reducer with the type indicated
          dispatch({ type: 'RegisterFailureUser', errors: error.errors })
        }
      )
  }
}
