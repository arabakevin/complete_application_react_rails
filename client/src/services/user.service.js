export const userService = {
  register
}

function register(user){
  const mandatoryOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify({
      user: user
    })
  }
  return fetch('/api/users', mandatoryOptions).then(handleResponse)
}

function handleResponse(response) {
  if (!response.ok) {
    return response.json().then(body => Promise.reject(body))
  }

  return response.status === 204 ? response : response.json()
}
