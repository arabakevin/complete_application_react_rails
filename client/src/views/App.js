import React from 'react'
import { connect } from 'react-redux'
import { AppRouter } from '../components/AppRouter'
import { Jumbotron, Col, Container, Row } from 'reactstrap'

import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {

  render(){
    return (
      <Jumbotron>
        <Container>
          <Row>
            <Col sm={{ size: 8, offset: 2 }}>
            <h3>Heello</h3>
            <AppRouter />
            </Col>
          </Row>
        </Container>
      </Jumbotron>
    )
  }
}

// connect used to connect the store
export default connect()(App);
