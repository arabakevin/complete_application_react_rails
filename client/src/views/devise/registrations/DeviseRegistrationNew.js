import React from 'react'
import { connect } from 'react-redux'

// bootstrap pour react
import { Col, Row } from 'reactstrap'

// import the actions here
import { userActions } from '../../../actions/user.actions'

class DeviseRegistrationNew extends React.Component {
  constructor(props) {
    super(props)

    // we initialize the state and the props
    this.state = {
      user : {
        email: '',
        password: ''
      },
      submitted: false
    }

    // bind take in relation with the class where is the method
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  //
  handleChange(event){
    const { name, value } = event.target
    const { user } = this.state
    console.log("event = " + event.target)
    console.log("name = " + name)
    console.log("value = " + value)
    console.log("user = " + user)
    console.log("JSON state")
    console.log(JSON.parse(JSON.stringify(this.state)))
    console.log("JSON user")
    console.log(JSON.parse(JSON.stringify(user)))
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    })
  }

  handleSubmit(event){
    event.preventDefault()

    this.setState({ submitted: true })
    const { user } = this.state
    if (user.email && user.password) {
      this.props.dispatch(userActions.register(user))
    }
  }

  render(){
    const { registering, errors } = this.props
    const { user, submitted } = this.state

    return (
      <Row>
        <Col md={{ size: 10, offset: 2}}>
          <h2>Register</h2>
          <form name="form" onSubmit={this.handleSubmit}>
            <div className={'form-group' + (submitted && (!user.email || (errors && errors.email)) ? ' has-error' : '')}>
              <label htmlFor="user_email">Email</label>
              <input type="text" className="form-control" name="email" id="user_email" value={user.email} onChange={this.handleChange} />
              {submitted && !user.email &&
                <div className="help-block">Email is required</div>
              }
              {submitted && errors && errors.email &&
                <div className="help-block">{errors.email}</div>
              }
            </div>
            <div className={'form-group' + (submitted && (!user.password || (errors && errors.password)) ? ' has-error' : '')}>
              <label htmlFor="user_password">Password</label>
              <input type="password" className="form-control" name="password" id="user_password" value={user.password} onChange={this.handleChange} />
              {submitted && !user.password &&
                <div className="help-block">Password is required</div>
              }
              {submitted && errors && errors.password &&
                <div className="help-block">{errors.password}</div>
              }
            </div>
            <div className="form-group">
              <button className="btn btn-primary">Register</button>
              {registering}
            </div>
          </form>
        </Col>
      </Row>
    )


  }

}

function mapStateToProps(state){
  return state
}

// we connect here the store and the state will be updated by the actions
// automatically your component should be informed of this modification.
export default connect(mapStateToProps)(DeviseRegistrationNew)
