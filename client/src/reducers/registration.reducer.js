
// A action file has just state and action in params.
export function registration (state ={}, actions){
  // we create a switch
  switch (actions.type){
    case 'RegisterRequestUser':
      return { registering: true }
    case 'RegisterSuccessUser':
      return {}
    case 'RegisterFailureUser':
      return { errors: actions.errors }
    // we have a default actions to just return the state.
    default:
      return state
  }
}
