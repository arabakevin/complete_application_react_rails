import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import App from './views/App'
import { store } from './stores'

// here we can add the store at us Application
// to allow to have the store in all components in the app
// provider share the store at all components of the app.

document.addEventListener('DOMContentLoaded', function() {
  render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  )
})
