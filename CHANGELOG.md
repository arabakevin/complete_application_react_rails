# complete_application_react_rails changelog

The latest version of this file can be found at the master branch of the
complete_application_react_rails repository.

v.1.0.1

- Remove the variables and methods not used.
- Clarify few comments

v.1.0.0

- Ruby on rails `Rails 5.2.1 ruby 2.5.1p57` (backend)
- React (frontend) `version 16.6.3`
- API to communication
- Devise
- jwt
- reactstrap `version 6.5.0`
- redux
- register method for a user.
