Rails.application.routes.draw do
  # ~~~~ Application API ~~~~

  scope :api, module: :api, constraints: { format: 'json' } do
  # We are defining the controllers paths avoding to search them from an `api`
  # folder.
  devise_for :users, controllers: {
    confirmations: 'devise/confirmations',
    passwords: 'devise/passwords',
    registrations: 'devise/registrations',
    sessions: 'sessions',
    unlocks: 'devise/unlocks'
  }
    # ~~~~ Application Resources ~~~~
  resources :users

    # Application root is required.
    root to: 'home#index'

  end
end
