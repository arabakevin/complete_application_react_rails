class AddsTokenColumnToUsersTable < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :tokens, :string
    add_index :users, :token
  end
end
