class ApplicationController < ActionController::Base
  # null_session doit être utilisé dans les contrôleurs de style API,
  # où vous n'avez aucune utilisation pour l'objet de session.
  protect_from_forgery with: :null_session

  # reset_session est destiné aux contrôleurs traditionnels.
  # protect_from_forgery with: :reset_session
end
