class SessionsController < Devise::SessionsController

  def create
    super do |resource|
      # here we verify if the user is sign_in with devise
      if user_signed_in?
        #current_user est une method devise
        resource.token = JWTCover.encode(user_id: current_user.id)
      end
    end
  end
end
