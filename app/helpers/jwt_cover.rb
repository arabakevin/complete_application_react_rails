module JWTCover
  extend self

  def encode (payload, expiration = nil )
    expiration ||= Rails.application.secrets.jwt_expiration_hours

    payload = payload.dup

    payload[:exp] = expiration.hours.from_now.to_i

    JWT.encode payload, Rails.application.secrets.jwt_secret
  end

  def decode(token)
    return if token.nil?

    decoded_jwt = JWT.decode token, Rails.application.secrets.jwt_secret

    decoded_jwt.first
  rescue StandardError
      Rails.logger.info("#############################")
      Rails.logger.info("We have a standard error when we decode the jwt token")
      Rails.logger.info("#############################")
  end
end
